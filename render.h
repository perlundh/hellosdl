#ifndef _TEXTUREH
#define _TEXTUREH

#include<SDL2/SDL_render.h>
#include<SDL2/SDL_image.h>

#define DEFAULT_INIT_FLAGS SDL_INIT_VIDEO
#define DEFAULT_WINDOW_FLAGS 0
#define DEFAULT_RENDERER_FLAGS SDL_RENDERER_ACCELERATED

char *dataDir;

SDL_Renderer *fbuffer;
SDL_Window *wind;

int initiate(char *wintitle);

int dispose();

void logSDLerror();

SDL_Texture *loadTexture(char *dir);
SDL_Texture *loadTextureToRenderer(char *dir, SDL_Renderer * ren);

/* 
 * Draws a texture at screen position (x, y) to the famebuffer.
 * The size of the drawn area will be equal to the texture size
 */
void drawTexture(SDL_Texture * tex, int x, int y);
void drawRotatedTexture(SDL_Texture * tex, int x, int y, int deg);
/*
 * Draws a texture at the position
 * specified by dest to the framebuffer
 */
void drawScaledTexture(SDL_Texture * tex, SDL_Rect * dest);
/*
 * Draws a tiled texture in the destination rectangle
 */
void drawTiledTexture(SDL_Texture * tex, SDL_Rect * dest);
/*
 * Draws a tiled texture to the framebuffer
 * The texture size will be scaled by sfac
 */
void drawScaledTiledTexture(SDL_Texture * tex, SDL_Rect * dest,
			    float sfac);
/*
 * Fills the framebuffer with a tiled background.
 * The size of the tiles will be equal to the texture size.
 */
void drawTiledBackground(SDL_Texture * tex);

#endif				/* _TEXTUREH */
