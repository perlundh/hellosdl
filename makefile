COMP=gcc
FLAGS=-Wall -g -std=c99
FILES=main.c state.c render.c
LIBS=-lSDL2 -lSDL2_image -lm
OUTPUT=hellosdl

build:
	$(COMP) $(FLAGS) $(FILES) $(LIBS) -o $(OUTPUT)
