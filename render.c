#include"render.h"
#include<string.h>
#include<math.h>

void drawTexture(SDL_Texture * tex, int x, int y)
{
	SDL_Rect dest;
	dest.x = x;
	dest.y = y;
	SDL_QueryTexture(tex, NULL, NULL, &dest.w, &dest.h);
	SDL_RenderCopy(fbuffer, tex, NULL, &dest);
}

void drawRotatedTexture(SDL_Texture * tex, int x, int y, int deg)
{
	SDL_Rect dest;
	dest.x = x;
	dest.y = y;
	SDL_QueryTexture(tex, NULL, NULL, &dest.w, &dest.h);
	SDL_RenderCopyEx(fbuffer, tex, NULL, &dest, (double)deg, NULL, SDL_FLIP_NONE);
}

void drawScaledTexture(SDL_Texture * tex, SDL_Rect * dest)
{
    SDL_RenderCopy(fbuffer, tex, NULL, dest);
}

void drawTiledTexture(SDL_Texture * tex, SDL_Rect * dest)
{
	drawScaledTiledTexture(tex, dest, 1);
}

void drawScaledTiledTexture(SDL_Texture * tex, SDL_Rect * dest, float sfac)
{
	SDL_Rect tdest;
	tdest.x = 0;
	tdest.y = 0;
	SDL_QueryTexture(tex, NULL, NULL, &tdest.w, &tdest.h);
	float wcount, hcount;
	wcount = dest->w / (float)tdest.w;
	hcount = dest->h / (float)tdest.h;

	for(int x = 0; x < wcount; ++x){
		tdest.x = x * tdest.w;
		for(int y = 0; y < hcount; ++y){
			tdest.y = y * tdest.h;
			if(wcount - x > 0 && hcount - y > 0){
				SDL_RenderCopy(fbuffer, tex, NULL, &tdest);
			}
			else {
				SDL_Rect src;
				src.x = 0;
				src.y = 0;
				src.w = floor(tdest.w * -(wcount - x));
				src.h = floor(tdest.w * -(hcount - y));
				tdest.w = src.w;
				tdest.h = src.h;
				SDL_RenderCopy(fbuffer, tex, &src, &tdest);
			}
		}
	}
}

void drawTiledBackground(SDL_Texture * tex)
{
	SDL_Rect scrn;
	scrn.x = 0;
	scrn.y = 0;
	SDL_GetWindowSize(wind, &scrn.w, &scrn.h);
	drawTiledTexture(tex, &scrn);
}

SDL_Texture *loadTextureToRenderer(char *dir, SDL_Renderer * ren)
{
    char d[50] = "";
    strcat(d, dataDir);
    strcat(d, "/");
    strcat(d, dir);
    SDL_Texture *rv = IMG_LoadTexture(ren, d);
    if (rv == NULL) {
	logSDLerror();
    }
    return rv;
}

SDL_Texture *loadTexture(char *dir)
{
    return loadTextureToRenderer(dir, fbuffer);
}

void logSDLerror()
{
    printf("SDL error: %s\n", SDL_GetError());
}

int initiate(char *wintitle)
{
    if (SDL_Init(DEFAULT_INIT_FLAGS) > 0) {
	logSDLerror();
	return -1;
    }
    wind =
	SDL_CreateWindow(wintitle, 0, 0, 255, 255, DEFAULT_WINDOW_FLAGS);
    if (wind == NULL) {
	logSDLerror();
	return -1;
    }
    fbuffer = SDL_CreateRenderer(wind, -1, DEFAULT_RENDERER_FLAGS);
    if (fbuffer == NULL) {
	logSDLerror();
	return -1;
    }
    dataDir = getenv("HOME");
    return 0;
}

int dispose()
{
    SDL_DestroyRenderer(fbuffer);
    SDL_DestroyWindow(wind);
    return 0;
}
