#include"state.h"
#include"render.h"

void initInput()
{
    kbstate = SDL_GetKeyboardState(NULL);
    mstate = SDL_GetMouseState(&mx, &my);
    dmx = 0;
    dmy = 0;
    dmwlx = 0;
    dmwly = 0;
    mwlx = 0;
    mwlx = 0;
}

void updateInput()
{
    SDL_PumpEvents();
    int prex = mx;
    int prey = my;
    mstate = SDL_GetMouseState(&mx, &my);
    dmx = prex - mx;
    dmy = prey - my;
}

void initState()
{
    initInput();
    winx = 0;
    winy = 0;
    winw = 0;
    winh = 0;
    winfocused = true;
    winminimized = false;
    scrnw = 0;
    scrnh = 0;
    lastTime = SDL_GetPerformanceCounter();
    dt = 0;
    SDL_GetWindowSize(wind, &winw, &winh);
}

void updateState()
{
	dt = (SDL_GetPerformanceCounter() - lastTime) / (float)SDL_GetPerformanceFrequency();
	lastTime = SDL_GetPerformanceCounter();
    SDL_Event cevnt;
    updateInput();
    while (SDL_PollEvent(&cevnt)) {
	switch (cevnt.type) {
	case SDL_WINDOWEVENT_MAXIMIZED:
	    winminimized = false;
	    break;
	case SDL_WINDOWEVENT_MINIMIZED:
	    winminimized = true;
	    break;
	case SDL_WINDOWEVENT_FOCUS_LOST:
	    winfocused = false;
	    break;
	case SDL_WINDOWEVENT_FOCUS_GAINED:
	    winfocused = true;
	    break;
	case SDL_WINDOWEVENT_MOVED:
	    winx = cevnt.window.data1;
	    winy = cevnt.window.data2;
	    break;
	case SDL_WINDOWEVENT_RESIZED:
	    winw = cevnt.window.data1;
	    winh = cevnt.window.data2;
	    break;
	case SDL_QUIT:
	    quitreq = true;
	    break;
	default:
	    break;
	}

    }
}
