#include<stdbool.h>
#include<math.h>
#include<SDL2/SDL.h>
#include<SDL2/SDL_scancode.h>
#include<SDL2/SDL_timer.h>

#include"state.h"
#include"render.h"


#define PI 3.14159265

int sign(float v)
{
	if(v > 0){
		return 1;
	}
	else if(v < 0){
		return -1;
	}
	else {
		return 0;
	}
}

int main()
{
    initiate("Moo moo moo!!");

    // SDL_SetRenderDrawColor(fbuffer, 255, 255 ,255 ,255 );

    SDL_Texture *bg = loadTexture("bg.png");
    SDL_Texture *tex = loadTexture("tex.png");

    initState();

    float posx = 0;
    float posy = 0;
    float angle = 0;
    float speed = 1000;
    float rotspeed = 180;
    float anglevel = 0;
    float velx = 0;
    float vely = 0;
    float veldec = 500;
    float angveldec = 145;

    while (!quitreq) {
	updateState();

	if (kbstate[SDL_SCANCODE_ESCAPE]) {
	    quitreq = true;
	}
	if (kbstate[SDL_SCANCODE_LEFT]) {
		anglevel -= rotspeed * dt;
	}
	if(kbstate[SDL_SCANCODE_RIGHT]){
		anglevel += rotspeed * dt;
	}
	if(kbstate[SDL_SCANCODE_DOWN]){
		float radang = (angle + 90) * PI / 180.0f;
		velx += cos(radang) * speed * dt;
		vely += sin(radang) * speed * dt;
	}
	if(kbstate[SDL_SCANCODE_UP]){
		float radang = (angle + 90) * PI / 180.0f;
		velx -= cos(radang) * speed * dt;
		vely -= sin(radang) * speed * dt;
	}
	angle += anglevel * dt;
	posx += velx * dt;
	posy += vely * dt;
	anglevel += angveldec * dt * -sign(anglevel);
	velx += veldec * dt * -sign(velx);
	vely += veldec * dt * -sign(vely);
	
	SDL_RenderClear(fbuffer);
	drawTiledBackground(bg);
	drawRotatedTexture(tex, round(posx), round(posy), round(angle));
	SDL_RenderPresent(fbuffer);
    }

    SDL_Quit();
    return 0;
}
