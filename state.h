#ifndef _INPUTH
#define _INPUTH

#include<SDL2/SDL_timer.h>
#include<SDL2/SDL_events.h>
#include<stdbool.h>

/* 	Input state
 ******************************************************************************
 */

const Uint8 *kbstate;
Uint32 mstate;
int mx;
int my;
int dmx;
int dmy;
int dmwlx;
int dmwly;
long int mwlx;
long int mwly;

void initInput();

void updateInput();

/*	Screen/window state
 ******************************************************************************
 */

/* Screen dimensions (framebuffer required dimensions) */
int scrnw;
int scrnh;

/* Window state
 * (One window only)
 */
int winx;
int winy;
int winw;
int winh;
bool winfocused;
bool winminimized;

/*
 * App state (frametimes etc)
 ******************************************************************************
 */
float dt;
Uint64 lastTime;
bool quitreq;

void initState();

void updateState();

#endif				/* _INPUTH */
